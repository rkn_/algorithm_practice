#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <string>
#include <map>
#include <tuple>
#include <math.h>
#include <deque>
#include <stack>
#include <queue>
 
using namespace std;
 
typedef long long ll;

struct idx {
    ll x;
    ll y;
};

int disp(vector<vector<char> > g,ll n,ll m){
    for(ll i = 0;i < n; i++){
        for(ll j = 0;j < m; j++){
            cout << g[i][j];
        }
        cout << endl;
    }
    cout << endl;
    return 0;
}

int solve(){
    ll n,m;
    cin >> n >> m;
    vector<vector<char> > g(n, vector<char> (m));
    queue<idx> que;
    // vector<vector<bool> > visit(n, vector<bool> (m, false));
    for(ll i = 0;i < n;i++){
        for(ll j = 0;j < m;j++){
            cin >> g[i][j];
        }
    }
    ll cnt = 0;
    for(ll i = 0;i < n; i++){
        for(ll j = 0;j < m; j++){
            if(g[i][j] == 'W'){
                idx tmp;
                cnt++;
                tmp.x = j;
                tmp.y = i;
                g[i][j] = '.';
                que.push(tmp);
                while(!que.empty()){
                    tmp = que.front();
                    que.pop();
                    ll up = tmp.y != 0 ? tmp.y - 1 : tmp.y;
                    ll down = tmp.y != n - 1 ? tmp.y + 1 : tmp.y;
                    ll left = tmp.x != 0 ? tmp.x - 1 : tmp.x;
                    ll right = tmp.x != m - 1 ? tmp.x + 1 : tmp.x;
                    for(ll k = up;k <= down; k++){
                        for(ll l = left;l <= right; l++){
                            if(g[k][l] == 'W'){
                                idx next;
                                next.y = k;
                                next.x = l;
                                que.push(next);
                                g[k][l] = '.';
                            }
                        }
                    }
                }
                // disp(g, n, m);
            }
        }
    }

    cout << cnt << endl;
    return 0;
}

int main(void){
    solve();
    return 0;
}