#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <string>
#include <map>
#include <tuple>
#include <math.h>
#include <deque>
#include <stack>
 
using namespace std;
 
typedef long long ll;

// 深さ優先探索
bool dfs(vector<ll> a, ll sum, ll i, ll k,ll n){
    // nに達した時に合計がkと等しいかを判断
    if(i == n){
        return sum == k;
    }
    // 足さない
    if(dfs(a, sum, i + 1, k, n)){
        return true;
    }
    // 足す
    if(dfs(a, sum + a[i], i + 1, k, n)){
        return true;
    }
    // 操作ができないので false
    return false;
}

int solve(){
    ll n,k;
    cin >> n >> k;
    vector<ll> a(n);
    for(ll i = 0;i < n;i++){
        cin >> a[i];
    }
    if(dfs(a, 0, 0, k, n)){
        cout << "Yes" << endl;
    }else{
        cout << "No" << endl;
    }
    return 0;
}
 
int main(void){
    solve();
    return 0;
}