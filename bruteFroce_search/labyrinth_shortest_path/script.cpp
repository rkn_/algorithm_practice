#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <string>
#include <map>
#include <tuple>
#include <math.h>
#include <deque>
#include <stack>
#include <queue>
 
using namespace std;
 
typedef long long ll;

struct idx {
    ll x;
    ll y;
};

int disp(vector<vector<ll> > g,ll n,ll m){
    for(ll i = 0;i < n; i++){
        for(ll j = 0;j < m; j++){
            cout << g[i][j] << " ";
        }
        cout << endl;
    }
    cout << endl;
    return 0;
}
// dfs 用
int dfssolve(){
    ll n,m;
    cin >> n >> m;
    vector<vector<char> > g(n, vector<char> (m));
    vector<vector<ll> > dist(n, vector<ll> (m));
    for(ll i = 0;i < n;i++){
        for(ll j = 0;j < m;j++){
            cin >> g[i][j];
        }
    }
    idx s;
    idx end;
    for(ll i = 0;i < n; i++){
        for(ll j = 0;j < m; j++){
            if(g[i][j] == 'S'){
                s.x = j;
                s.y = i;
            }
            if(g[i][j] == 'G'){
                end.x = j;
                end.y = i;
            }
        }
    }
    stack<idx> st;
    st.push(s);
    while(!st.empty()){
        // disp(dist, n, m);
        ll nowx = st.top().x;
        ll nowy = st.top().y;
        if(nowx == end.x && nowy == end.y){
            cout << dist[nowy][nowx] << endl;
            return 0;
        }
        ll up = nowy != 0 ? nowy - 1 : nowy;
        ll down = nowy != n - 1 ? nowy + 1 : nowy;
        ll left = nowx != 0 ? nowx - 1 : nowx;
        ll right = nowx != m - 1 ? nowx + 1 : nowx;
        idx next;
        if(g[nowy][left] == '.'|| g[nowy][left] == 'G'){
            g[nowy][left] = '#';
            dist[nowy][left] = dist[nowy][nowx] + 1;
            next.y = nowy;
            next.x = left;
            st.push(next);
        }else if(g[nowy][right] == '.' || g[nowy][right] == 'G'){
            g[nowy][right] = '#';
            dist[nowy][right] = dist[nowy][nowx] + 1;
            next.x = right;
            next.y = nowy;
            st.push(next);
        }else if(g[up][nowx] == '.' || g[up][nowx] == 'G'){
            g[up][nowx] = '#';
            dist[up][nowx] = dist[nowy][nowx] + 1;
            next.x = nowx;
            next.y = up;
            st.push(next);
        }else if(g[down][nowx] == '.' || g[down][nowx] == 'G'){
            g[down][nowx] = '#';
            dist[down][nowx] = dist[nowy][nowx] + 1;
            next.x = nowx;
            next.y = down;
            st.push(next);
        }else{
            st.pop();
        }
    }
    return 0;
}

// bfsで解いた
int solve(){
    ll n,m;
    cin >> n >> m;
    vector<vector<char> > g(n, vector<char> (m));
    vector<vector<ll> > dist(n, vector<ll> (m));
    queue<idx> que;
    // vector<vector<bool> > visit(n, vector<bool> (m, false));
    for(ll i = 0;i < n;i++){
        for(ll j = 0;j < m;j++){
            cin >> g[i][j];
        }
    }
    for(ll i = 0;i < n; i++){
        for(ll j = 0;j < m; j++){
            if(g[i][j] == 'S'){
                idx tmp;
                tmp.x = j;
                tmp.y = i;
                g[i][j] = '#';
                que.push(tmp);
                while(!que.empty()){
                    tmp = que.front();
                    que.pop();
                    ll nowx = tmp.x;
                    ll nowy = tmp.y;
                    if(g[nowy][nowx] == 'G'){
                        cout << dist[nowy][nowx] << endl;
                        return 0;
                    }
                    ll up = tmp.y != 0 ? tmp.y - 1 : tmp.y;
                    ll down = tmp.y != n - 1 ? tmp.y + 1 : tmp.y;
                    ll left = tmp.x != 0 ? tmp.x - 1 : tmp.x;
                    ll right = tmp.x != m - 1 ? tmp.x + 1 : tmp.x;
                    idx next;
                    if(g[nowy][left] == '.' || g[nowy][left] == 'G'){
                        next.y = nowy;
                        next.x = left;
                        g[nowy][left] = g[nowy][left] == '.' ? '#' : 'G';
                        dist[nowy][left] = dist[nowy][nowx] + 1;
                        que.push(next);
                    }
                    if(g[nowy][right] == '.' || g[nowy][right] == 'G'){
                        next.y = nowy;
                        next.x = right;
                        g[nowy][right] = g[nowy][right] == '.' ? '#' : 'G';
                        dist[nowy][right] = dist[nowy][nowx] + 1;
                        que.push(next);
                    }
                    if(g[up][nowx] == '.' || g[up][nowx] == 'G'){
                        next.y = up;
                        next.x = nowx;
                        g[up][nowx] = g[up][nowx] == '.' ? '#' : 'G';
                        dist[up][nowx] = dist[nowy][nowx] + 1;
                        que.push(next);
                    }
                    if(g[down][nowx] == '.' || g[down][nowx] == 'G'){
                        next.y = down;
                        next.x = nowx;
                        g[down][nowx] = g[down][nowx] == '.' ? '#' : 'G';
                        dist[down][nowx] = dist[nowy][nowx] + 1;
                        que.push(next);
                    }
                }
            }
        }
    }
    return 0;
}

int main(void){
    // solve();
    dfssolve();
    return 0;
}