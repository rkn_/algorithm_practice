#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <string>
#include <map>
#include <tuple>
#include <math.h>
#include <deque>
#include <stack>
 
using namespace std;
 
typedef long long ll;

struct item {
    ll val;
    ll weight;
};

int disp(vector<vector<ll> > dp){
    for(ll i = 0;i < dp.size(); i++){
        for(ll j = 0;j < dp[i].size(); j++){
            cout << dp[i][j] << " ";
        }
        cout << endl;
    }
    return 0;
}

int solve(){
    ll n, w;
    cin >> n >> w;
    vector<vector<ll> > dp(n + 1, vector<ll> (w + 1, 0));
    vector<item> items(n);
    for(ll i = 0;i < n; i++){
        cin >> items[i].val >> items[i].weight;
    }
    for(ll i = 0;i < n; i++){
        for(ll j = 0;j <= w; j++){
            if(j >= items[i].weight){
                // dp[i + 1][j - items[i].weight]までの計算は既に完了しているため、その時の値を再利用
                // jは列移動なので同じ列の値を再利用することにより、個数制限無しナップザックが解ける
                dp[i + 1][j] = max(dp[i][j], dp[i + 1][j - items[i].weight] + items[i].val);
            }else{
                dp[i + 1][j] = dp[i][j];
            }
        }
    }
    // disp(dp);
    cout << dp[n][w] << endl;
    return 0;
}

int main(void){
    solve();
    return 0;
}