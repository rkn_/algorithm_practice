#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <string>
#include <map>
#include <tuple>
#include <math.h>
#include <deque>
#include <stack>
 
using namespace std;
 
typedef long long ll;

struct item {
    ll a;
    ll m;
};

int disp(vector<vector<ll> > dp){
    for(ll i = 0;i < dp.size(); i++){
        for(ll j = 0;j < dp[i].size(); j++){
            cout << dp[i][j] << " ";
        }
        cout << endl;
    }
    return 0;
}

int solve(){
    ll n;
    cin >> n;
    vector<ll> dp(n, 0);
    vector<ll> a(n);
    for(ll i = 0;i < n; i++){
        cin >> a[i];
    }
    ll ans = 0;
    for(ll i = 0;i < n; i++){
        dp[i] = 1;
        for(ll j = 0;j < i; j++){
            if(a[i] > a[j]){
                // 値の更新
                dp[i] = max(dp[i], dp[j] + 1);
            }
        }
        ans = max(ans, dp[i]);
    }
    cout << ans << endl;
    return 0;
}

int main(void){
    solve();
    return 0;
}