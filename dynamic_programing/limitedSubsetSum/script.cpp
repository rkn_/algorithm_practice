#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <string>
#include <map>
#include <tuple>
#include <math.h>
#include <deque>
#include <stack>
 
using namespace std;
 
typedef long long ll;

struct item {
    ll a;
    ll m;
};

int disp(vector<vector<ll> > dp){
    for(ll i = 0;i < dp.size(); i++){
        for(ll j = 0;j < dp[i].size(); j++){
            cout << dp[i][j] << " ";
        }
        cout << endl;
    }
    return 0;
}

int solve(){
    ll n, k;
    cin >> n >> k;
    vector<vector<ll> > dp(n + 1, vector<ll> (k + 1, -1));
    vector<item> items(n);
    for(ll i = 0;i < n; i++){
        cin >> items[i].a >> items[i].m;
    }
    dp[0][0] = 0;
    for(ll i = 0;i < n; i++){
        for(ll j = 0;j <= k; j++){
            if(dp[i][j] >= 0){
                // 0以上の時は残りの枚数を代入
                dp[i + 1][j] = items[i].m;
            }else if(j < items[i].a || dp[i + 1][j - items[i].a] <= 0){
                // aの値が j 以上の時はその値を作れない
                // 残りの枚数が0の以下の時には作れないため、-1
                dp[i + 1][j] = -1;
            }else{
                // それ以外の時は、以前の枚数から1枚多く使って和を作れるから -1 をする
                dp[i + 1][j] = dp[i + 1][j - items[i].a] - 1;
            }
        }
    }
    // disp(dp);
    if(dp[n][k] >= 0){
        cout << "Yes" << endl;
    }else{
        cout << "No" << endl;
    }
    return 0;
}

int main(void){
    solve();
    return 0;
}