#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <string>
#include <map>
#include <tuple>
#include <math.h>
#include <deque>
#include <stack>
 
using namespace std;
 
typedef long long ll;

struct item {
    ll a;
    ll m;
};

int disp(vector<vector<ll> > dp){
    for(ll i = 0;i < dp.size(); i++){
        for(ll j = 0;j < dp[i].size(); j++){
            cout << dp[i][j] << " ";
        }
        cout << endl;
    }
    return 0;
}

int solve(){
    ll n, m, M;
    cin >> n >> m >> M;
    vector<vector<ll> > dp(m + 1, vector<ll> (n + 1, 0));
    dp[0][0] = 1;
    for(ll i = 1; i <= m; i++){
        for(ll j = 0;j <= n; j++){
            if(j >= i){
                // 分割できる時
                // 前の分割数と、前の分割数を含まない分割の数の和を算出
                // j - i は j を i 分割した時の数を指定できる
                // j - i は ドミノ式に値を計算していくので 9 - 3 の時は 6 - 3 , 3 - 3の計算もしている
                dp[i][j] = (dp[i - 1][j] + dp[i][j - i]) % M;
            }else{
                // 分割できないため、前の値を再利用
                dp[i][j] = dp[i - 1][j];
            }
        }
    }
    // disp(dp);
    cout << dp[m][n] << endl;
    return 0;
}

int main(void){
    solve();
    return 0;
}