#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <string>
#include <map>
#include <tuple>
#include <math.h>
#include <deque>
#include <stack>
 
using namespace std;
 
typedef long long ll;

struct item {
    ll val;
    ll weight;
};

int disp(vector<vector<ll> > dp){
    for(ll i = 0;i < dp.size(); i++){
        for(ll j = 0;j < dp[i].size(); j++){
            cout << dp[i][j] << " ";
        }
        cout << endl;
    }
    return 0;
}

int solve(){
    ll n, m;
    cin >> n >> m;
    string s,t;
    cin >> s >> t;
    string ans = "";
    vector<vector<ll> > dp(n + 1, vector<ll> (m + 1, 0));

    for(ll i = 0;i < n;i++){
        for(ll j = 0;j < m; j++){
            // 次の文字列までの個数を更新
            if(s[i] == t[j]){ // 等しい時は1つ増やす
                dp[i + 1][j + 1] = dp[i][j] + 1;
                ans = ans + s[i];
            } else { // 等しくない時は、i, j までの共通の数の多い方で更新を行う
                dp[i + 1][j + 1] = max(dp[i][j + 1], dp[i + 1][j]);
            }
        }
    }

    // disp(dp);

    cout << dp[n][m] << endl;
    cout << ans << endl;
    return 0;
}

int main(void){
    solve();
    return 0;
}