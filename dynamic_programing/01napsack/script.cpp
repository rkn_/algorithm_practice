#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <string>
#include <map>
#include <tuple>
#include <math.h>
#include <deque>
#include <stack>
 
using namespace std;
 
typedef long long ll;

struct item {
    ll val;
    ll weight;
};

int disp(vector<vector<ll> > dp){
    for(ll i = 0;i < dp.size(); i++){
        for(ll j = 0;j < dp[i].size(); j++){
            cout << dp[i][j] << " ";
        }
        cout << endl;
    }
    return 0;
}

int solve(){
    ll n, w;
    cin >> n >> w;
    vector<item> items(n);
    vector<vector<ll> > dp(n, vector<ll> (w + 1, 0));
    
    for(ll i = 0;i < n; i++){
        cin >> items[i].val >> items[i].weight;
    }

    for(ll i = 0;i < n - 1; i++){
        for(ll j = 0;j < w + 1; j++){
            if(j >= items[i].weight){
                dp[i + 1][j] = max(dp[i][j], dp[i][j - items[i].weight] + items[i].val);
            }else{
                dp[i + 1][j] = dp[i][j];
            }
        }
    }
    // disp(dp);

    cout << dp[n - 1][w] << endl;
    return 0;
}

int main(void){
    solve();
    return 0;
}