#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <string>
#include <map>
#include <tuple>
#include <math.h>
#include <deque>
#include <stack>
 
using namespace std;
 
typedef long long ll;

struct work {
    ll s;
    ll e;
};

bool evalt(work w, work next){
    return w.e < next.e;
}

int solve(){
    ll n;
    cin >> n;
    vector<work> works(n);
    for(ll i = 0;i < n; i++){
        work w;
        cin >> w.s >> w.e;
        works[i] = w;
    }
    sort(works.begin(), works.end(), evalt);
    ll now = 0;
    ll ans = 0;
    for(ll i = 0;i < n; i++){
        if(works[i].s >= now){
            now = works[i].e;
            ans++;
        }
    }
    cout << ans << endl;
    return 0;
}
 
int main(void){
    solve();
    return 0;
}