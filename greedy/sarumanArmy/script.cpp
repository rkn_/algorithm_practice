#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <string>
#include <map>
#include <tuple>
#include <math.h>
#include <deque>
#include <stack>


using namespace std;


typedef long long ll;

int solve(){
    ll n,r, mx;
    cin >> n >> r;
    vector<ll> p(n);
    mx = -1;
    for(ll i = 0;i < n; i++){
        cin >> p[i];
        mx = max(mx,p[i]);
    }
    ll le = (-1) * r;
    ll ri = r;
    ll covered = 0;
    ll ans = 0;
    for(ll i = 0;i <= mx + 1; i++){
        ll left = i + le;
        ll right = i + ri;
        ll cover = 0;
        for(ll j = 0;j < n; j++){
            if(left <= p[j] && p[j] <= right){
                cover++;
            }
        }
        if(covered <= cover){
            covered = cover;
        }else{
            ans++;
            i = right;
            covered = 0;
        }
    }
    cout << ans << endl;
    return 0;
}


int main(void){
    solve();
    return 0;
}