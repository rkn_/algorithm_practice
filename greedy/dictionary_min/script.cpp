#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <string>
#include <map>
#include <tuple>
#include <math.h>
#include <deque>
#include <stack>


using namespace std;


typedef long long ll;

int solve(){
    ll n;
    string s,t;
    cin >> n;
    cin >> s;
    t = "";
    ll r,l;
    l = 0;
    r = n - 1;
    while(l <= r){
        if(s[l] < s[r]){
            t = t + s[l];
            l++;
        }else if(s[l] > s[r]){
            t = t + s[r];
            r--;
        }else{
            // 逆さにして同じ時は判定
            string reverse = "";
            string currect = "";
            for(ll i = l;i <= r; i++){
                reverse = reverse + s[r - i];
                currect = currect + s[i];
            }
            if(currect < reverse){
                t = t + s[l];
                l++;
            }else{
                t = t + s[r];
                r--;
            }
        }
    }
    cout << t << endl;
    return 0;
}


int main(void){
    solve();
    return 0;
}