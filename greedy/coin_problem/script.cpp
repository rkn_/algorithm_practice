#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <string>
#include <map>
#include <tuple>
#include <math.h>
#include <deque>
#include <stack>
 
using namespace std;
 
typedef long long ll;

struct coin {
    ll cnt;
    ll val;
};

int solve(){
    ll a;
    vector<coin> coins(6);
    int vals[6] = { 1, 5, 10, 50, 100, 500 };
    for(ll i = 0;i < 6; i++){
        coin tmp;
        cin >> tmp.cnt;
        tmp.val = vals[i];
        coins[i] = tmp;
    }
    cin >> a;
    ll cnt = 0;
    for(ll i = 5;i >= 0; i--){
        if(a > 0){
            ll num = a / coins[i].val;
            num = num <= coins[i].cnt ? num : coins[i].cnt;
            cnt += num;
            a -= num * coins[i].val;
        }
        // cout << a << endl;
    }
    cout << cnt << endl;
    return 0;
}
 
int main(void){
    solve();
    return 0;
}